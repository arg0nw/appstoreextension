//
//  ViewController.swift
//  AppExtn
//
//  Created by Aruna Wickramasinghe on 10/4/18.
//  Copyright © 2018 Aruna Wickramasinghe. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource , UITableViewDelegate, UISearchBarDelegate{
   
    
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var SearchBar: UISearchBar!
    
    var initGames = [Game]()
    var Games = [Game]()
    
    var activityIndicator :UIActivityIndicatorView = UIActivityIndicatorView()
    
    var endPointUrl = "https://itunes.apple.com/search"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Games = initGames
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return initGames.count
    }
    
    private func setUpSearchBar(){
        SearchBar.delegate = self
    }
    
    func searchUrl(searchText:String) -> String{
        
        let url = "\(endPointUrl)?term=\(searchText)&limit=10&entity=software"
        
        //let urlString = URL(string : url)
        return url
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell else{
            return UITableViewCell()
        }
        
        
        cell.GameNam.text = Games[indexPath.row].trackName
        cell.GameOwner.text = Games[indexPath.row].sellerName
        
        if let url = URL(string: Games[indexPath.row].artworkUrl512){
            do {
                let data = try Data(contentsOf: url)
                cell.GameThumbnail.image=UIImage(data: data)
            }catch let err{
                print(err)
            }
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    

    func searchBarSearchButtonClicked(_ searchBar : UISearchBar){
        startActivityIndicator()
        if !(searchBar.text?.isEmpty)!{
            let urlString = searchUrl(searchText: searchBar.text!)
            
            guard let url = URL(string: urlString) as URL? else{
                print("Issue in URL")
                return
            }
            Games = [Game]()
            Games=initGames
            fetchAppsData(url)
           
        }
        TableView.reloadData();
        stopActivityIndicator()
    }
    
    func startActivityIndicator(){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func imageDownloader(_ url :String)-> Data{
        
        let urli = URL(string: url)
        
        var loadedImage = Data()
        let Task = URLSession.shared.dataTask(with: urli!) { (data, response, error) in
            if error == nil{
                loadedImage = data!
            }
        }
        
        Task.resume()
        return loadedImage
    }
    
    func fetchAppsData(_ url:URL){
    
        let fetchiTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else{
                print(error!)
                return
            }
            
            guard let content = data else{
                print("No data found")
                return
            }
            do{
                let json = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject

                let results = (json["results"] as AnyObject)

                self.initGames = [Game]()
                for res in results as! [AnyObject]{

                    let game = Game(name: (res["trackName"] as AnyObject) as! String, sellerName: (res["sellerName"] as AnyObject) as! String, primGen: (res["primaryGenreName"] as AnyObject) as! String, gen: (res["genres"] as AnyObject) as! [String],thumbnail: (res["artworkUrl512"] as AnyObject) as! String)

                    print(game.sellerName)
                    
                    self.initGames.append(game)

                }
                print("Done")
                
            }catch {
                print ("Error in JSON Parser!")
            }

        }
        
        fetchiTask.resume()
        
    }

}


class Game {
    var trackName : String
    var sellerName : String
    var primaryGenreName:String
    var genres : [String]
    var artworkUrl512:String
    
    init(name:String, sellerName:String, primGen:String, gen : [String], thumbnail:String) {
        
        self.trackName=name
        self.sellerName = sellerName
        self.primaryGenreName = primGen
        self.genres = gen
        self.artworkUrl512 = thumbnail
        
    }
}
