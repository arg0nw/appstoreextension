//
//  TableCell.swift
//  AppExtn
//
//  Created by Aruna Wickramasinghe on 10/4/18.
//  Copyright © 2018 Aruna Wickramasinghe. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    
    @IBOutlet weak var GameNam: UILabel!
    @IBOutlet weak var GameOwner: UILabel!
    @IBOutlet weak var GameThumbnail: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
